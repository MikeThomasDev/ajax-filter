            <footer class="standard-footer">

                <div class="main-footer">

                    <div class="footer-item footer-item-1">
                    
                        <img class="footer-logo" src="/wp-content/uploads/main-img/logo.png" alt="Logo">

                        <div itemscope itemtype="http://schema.org/Organization">
								
                            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <span itemprop="name">Crillon Importers Ltd.</span> <span itemprop="streetAddress">The Atrium<br> 80 Route 4 East,</span> <span itemprop="addressLocality">Paramus,</span> <span itemprop="addressRegion">New Jersey</span> <span itemprop="postalCode">07652</span><br>
                            </div>

                        </div>

                    </div><!-- .footer-item-1 -->

                    <div class="footer-item footer-social">

                        <a href="https://linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a>

                        <a href="https://linkedin.com/">LinkedIn</a>
                    
                    </div>
                
                </div><!-- .main-footer -->
            
            </footer>

        </main><!-- #site-content -->
    
    </div><!-- main-wrapper -->

    <?php 
    
        wp_footer(); 
    
    ?>

    </body>

</html>
