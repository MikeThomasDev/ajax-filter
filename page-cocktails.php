<?php
/*
Template Name: Crillon cocktails
*/

get_header();
?>

    <div class="main-content lm-contact-page">

        <div class="page-header">

            <h1>Cocktails</h1>

        </div><!-- .page-header -->

        <div class="cocktails-filter crillon-posts-wrapper page-header">
            <h1>Custom Ajax Filter</h1>

            <?php

                $cat_args = array(
                    'type'                     => 'cocktail',
                    'child_of'                 => 0,
                    'parent'                   => '',
                    'orderby'                  => 'name',
                    'order'                    => 'ASC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'taxonomy'                 => 'brand',
                    'pad_counts'               => false 
                );

                $categories = get_categories($cat_args);
                echo '<ul>
                    <li class="js-filter-item"><a href="">All</a></li>';

                foreach ($categories as $cat) {
                    $url = get_term_link($cat);?>
                    <li class="js-filter-item"><a data-category="<?= $cat->term_id; ?>" href="<?= get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>
                    <?php
                }
                echo '</ul>';
            ?>

        </div><!-- .cocktails-filter -->

        <div id="cocktails-posts" class="crillon-posts-wrapper lm-grid-4-posts">

        <?php

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $args = array(
                'post_type' => 'cocktail',
                'posts_per_page' => 8,
                'paged' => $paged
            );

            $query = new WP_Query( $args );


            if ( $query->have_posts() ) :

            while ( $query->have_posts() ) : $query->the_post() ; ?>

            <article id="post-<?php the_ID(); ?>" class="lm-wrap">
                
                <div class="lm-post-content">
                
                    <div class="lm-post-thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url($cocktail -> ID, 'full'); ?>');"></div>
                    
                    <h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ; ?></a></h2>
                
                </div> <!-- .lm-post-content -->

            </article>

            <?php endwhile; ?>

            <div class="pagination">
                <?php
                $big = 999999999; // need an unlikely integer
                
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
                    'total'   => $query->max_num_pages,
                    'mid_size'        => 2,
                    'prev_text'       => __('PREV'),
                    'next_text'       => __('NEXT')
                ) );
                ?>
            </div>
            
            <?php endif; 
                  wp_reset_postdata();
            ?>

        </div><!-- #cocktails-posts -->

    </div><!-- .main-content -->

<?php
get_footer();