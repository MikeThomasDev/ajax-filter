<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $GLOBALS['pageTitle']; ?></title>
    <?php if (isset($GLOBALS['meta_description'])) { echo $GLOBALS['meta_description']; } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">
    <meta name="theme-color" content="#551317"/>
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="https://use.typekit.net/yhg7itg.css">
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@600&family=IM+Fell+French+Canon+SC&family=Playfair+Display:ital,wght@1,600&display=swap" rel="stylesheet">



    <!-- WP HEAD HOOK -->
	<?php do_action('wp_head'); ?>
    
  </head>

    <body <?php body_class(); ?>>

      <?php get_template_part( 'template-parts/content', 'age-gate' ); ?>

      <div class="main-wrapper">

        <header id="main-header" class="<?php if (isset($GLOBALS['dark-menu'])) { echo 'dark-menu'; }; ?>">
        
          <nav>
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/about/">About</a></li>
              <li class="has-sub-menu"><a href="/brands/">Brands</a>

                  <div class="sub-menu-wrapper">

                      <ul class="main-sub-menu">

                        <li class="mega-menu-item parent-item"><a class="title" href="/brand/barbancourt/">Barbancourt</a>
                          
                            <ul class="parent-item-menu">
                              <li class="parent-child-item"><a href="/brand/barbancourt/?bottle=1">15 years old, Estate Reserve</a></li>
                              <li class="parent-child-item"><a href="/brand/barbancourt/?bottle=2">5 Star, aged 8 years</a></li>
                              <li class="parent-child-item"><a href="/brand/barbancourt/?bottle=3">3 Star, aged 4 years</a></li>
                              <li class="parent-child-item"><a href="/brand/barbancourt/?bottle=4">White</a></li>
                              <li class="parent-child-item"><a href="/brand/barbancourt/?bottle=5">Pango Rhum</a></li>
                            </ul><!-- #Sub-sub-menu -->

                        </li>

                        <li class="mega-menu-item parent-item"><a class="title" href="/brand/absinthe/">Absinthe</a>

                              <ul class="parent-item-menu">
                                <li class="parent-child-item"><a href="/brand/absinthe/?bottle=1">ABSENTE Absinthe Refined</a></li>
                                <li class="parent-child-item"><a href="/brand/absinthe/?bottle=2">GRANDE ABSENTE Absinthe Originale</a></li>
                                <li class="parent-child-item"><a href="/brand/absinthe/?bottle=3">Absinthe Ordinaire</a></li>
                              </ul><!-- #Sub-sub-menu -->

                        </li>
						  
                        <li class="mega-menu-item parent-item"><a href="/brand/magellan/">Magellan Gin</a></li>
                        
                        <li class="mega-menu-item parent-item"><a class="title dead-link" href="#">FRENCH DISTILLED SPECIALTIES</a>

                              <ul class="parent-item-menu">
                                <li class="parent-child-item"><a href="/brand/pastis-henri-bardouin/">Pastis Henri Bardouin</a></li>
                                <li class="parent-child-item"><a href="/brand/belle-paire/">Belle Paire</a></li>
                                <li class="parent-child-item"><a href="/brand/farigoule/">Farigoule</a></li>
                                <li class="parent-child-item"><a href="/brand/elisir-mp-roux/">Elisir M.P. Roux</a></li>
                                <li class="parent-child-item"><a href="/brand/absentroux/">Absentroux</a></li>
                                <li class="parent-child-item"><a href="/brand/rin-quin-quin/">Rin Quin Quin</a></li>
                              </ul><!-- #Sub-sub-menu -->

                        </li>
  
                      </ul>

                  </div><!-- .sub-menu-wrapper --> 
              
              </li><!--.HAS-sub-menu -->
              
              <li><a href="/brand-locator/">Where To Buy</a></li>
              <li class="nav-logo"><a href="/"><img src="/wp-content/uploads/main-img/logo.png" alt="Logo"></a></li>
              <li><a href="/sales-tools/">Sales Tool</a></li>
              <li><a href="/news-and-events/">News &amp; Events</a></li>
              <li><a href="/contact/">Contact Us</a></li>

            </ul>
          </nav>

          <div id="mobile_nav_menu">
            <div class="menu-container for-responsive">

                <a href="<?php echo home_url( '/' ); ?>" ><img class="logo" src="/wp-content/uploads/main-img/logo.png" alt="Logo"></a>
                
                <span class="main-burger-btn"><i class="fas fa-bars"></i></span>

                <div class="mobile_nav closed">
                      <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_class' => 'header_menu' ) );?>
                </div>

            </div>
        </div>
        
        </header><!-- #side-header -->

        <main id="site-content" role="main">