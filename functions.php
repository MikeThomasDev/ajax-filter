<?php

// Global Variables 

// Ajax Filter Cocktails

add_action( 'wp_ajax_nopriv_filter', 'filter_ajax' );
add_action( 'wp_ajax_filter', 'filter_ajax' );

function filter_ajax() {

    $category = $_POST['category'];

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'cocktail',
		'posts_per_page' => 8,
		'paged' => $paged
	);

	if(isset($category)) {
		$args['category__in'] = array($category);
	}

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) :

	while ( $query->have_posts() ) : $query->the_post() ; ?>

	<article id="post-<?php the_ID(); ?>" class="lm-wrap">
		
		<div class="lm-post-content">
		
			<div class="lm-post-thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url($cocktail -> ID, 'full'); ?>');"></div>
			
			<h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ; ?></a></h2>
		
		</div> <!-- .lm-post-content -->

	</article>

	<?php endwhile; ?>

	<div class="pagination">
		<?php
		$big = 999999999; // need an unlikely integer
		
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => $paged,
			'total'   => $query->max_num_pages,
			'mid_size'        => 2,
			'prev_text'       => __('PREV'),
			'next_text'       => __('NEXT')
		) );
		?>
	</div>
	
	<?php endif; 
		  wp_reset_postdata();

    die();
}

// Load in our CSS
function crillon_styles() {	

	wp_enqueue_style( 'fontawesome-css', get_stylesheet_directory_uri() . '/fontawesome/css/all.css', [], time(), 'all' );
	wp_enqueue_style( 'flickity-css', get_stylesheet_directory_uri() . '/plugins/flickity/flickity.css', ['fontawesome-css'], time(), 'all' );
	wp_enqueue_style( 'main-css', get_stylesheet_directory_uri() . '/assets/css/main.css?version=2.0', ['flickity-css'], time(), 'all' );
}

add_action( 'wp_enqueue_scripts', 'crillon_styles' );


// Load in our JS
function crillon_scripts() {

	wp_enqueue_script( 'cookie-js', get_stylesheet_directory_uri() . '/plugins/jquery.cookie.js', array('jquery'), time(), true);
	wp_enqueue_script( 'flickity-js', get_stylesheet_directory_uri() . '/plugins/flickity/flickity.pkgd.min.js', array('jquery'), time(), true);
	wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), time(), true);
	wp_enqueue_script( 'ajax', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), time(), true);

    wp_localize_script('ajax', 'wpAjax',
        array('ajaxUrl' => admin_url('admin-ajax.php'))
    );
	
}

add_action( 'wp_enqueue_scripts', 'crillon_scripts', 30, 1 );

remove_filter('the_content', 'wpautop'); //stop wp from adding p tags
remove_filter( 'the_excerpt', 'wpautop' ); //stop from adding br tags



/****************************************************
***************** ADD THEME SUPPORT *****************
****************************************************/
/* Add theme options page for ACF */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');

/* Add Google Maps Support */

// Method 2: Setting.
function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyDxegGphNj2eP_YFypq6afNJJol0PI9TYY');
}
add_action('acf/init', 'my_acf_init');


/****************************************************
******************** REGISTER MENU ******************
****************************************************/
function register_my_menus() {
	register_nav_menus(
	  array(
		'header-menu' => __( 'Header Menu' ),
		'extra-menu' => __( 'Secondary Menu' )
	  )
	);
  }
  add_action( 'init', 'register_my_menus' );
  
/****************************************************
*********** REGISTER IMAGE THUMBNAIL ****************
****************************************************/

function mytheme_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'mytheme_post_thumbnails' );

/****************************************************
************* Resize post thumbnail *****************
****************************************************/

add_image_size( 'custom-post-medium-size', 600, 600, true ); 
add_image_size( 'cocktail-custom-size', 353, 314, true ); 
add_image_size( 'cocktail-lightbox-size', 1080, 1080, true ); 


  