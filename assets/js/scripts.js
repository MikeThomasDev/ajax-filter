ajaxFilter = function() {

    $ = jQuery.noConflict();

    console.log('hello');

    $(document).on('click', '.js-filter-item > a', function(e){

        e.preventDefault();

        var category = $(this).data('category');

        $.ajax({
            url: wpAjax.ajaxUrl,
            data: { action: 'filter', category: category},
            type: 'post',
            success: function(result) {
                $('#cocktails-posts').html(result);
                //console.log(result);
            },
            error: function(result) {
                console.warn(result);
            }
        });

    });

}

jQuery(document).ready(ajaxFilter);